(defpackage sudoku
  (:use :cl :rutilsx)
  (:export read-grid *sample-input*))

(in-package :sudoku)
(named-readtables:in-readtable rutils-readtable)

(defparameter *input-size* 81)

(defparameter *sample-input* ".......1.4.........2...........5.4.7..8...3....1.9....3..4..2...5.1........8.6...")

(defstruct cell
  ctype cvalue)

(defparameter *valid-numbers* (mapcar #'digit-char (range 0 10)))

(defun get-cell (c)
  (case c
    (#\. (make-cell :ctype :possible :cvalue (range 0 10)))
    (#.*valid-numbers* (make-cell :ctype :fixed :cvalue (digit-char-p c)))
    (t "ERROR")))

(defun good-char-p (c)
  (or (eql #\. c)
      (member c *valid-numbers*)))

(defun good-input-p (input)
  (and (every #'good-char-p input)
       (= *input-size* (length input))))

(defun batches (items count)
  (labels ((batch-helper (acc l)
             (if (not (emptyp l))
                 (let ((sublist (take count l))
                       (remainder (nthcdr count l)))
                   (if (emptyp acc)
                       (batch-helper (list sublist) remainder)
                     (batch-helper (append acc (list sublist)) remainder)))
               acc)))
    (batch-helper '() items)))

(defun read-grid (input)
  (let* ((input-list (coerce input 'list))
	 (all-cells (mapcar #'get-cell input-list)))
    (batches all-cells 9)))

(defun show-cell (c)
  (case (cell-ctype c)
    (:fixed    (format t " ~A " (cell-cvalue c)))
    (:possible (format t " . "))))

(defun show-row (r)
  (mapcar  #'show-cell r)
  (terpri))

(defun show-grid (grid &key show-possibles)
  (labels ((show-cell (c)
	     (if show-possibles
		 (case (cell-ctype c)
		   (:fixed    (format t "     ~A     " (cell-cvalue c)))
		   (:possible (format t " ~{~A~} " (cell-cvalue c))))
		 (case (cell-ctype c)
		   (:fixed    (format t " ~A " (cell-cvalue c)))
		   (:possible (format t " . ")))))
	   (show-row (r)
	     (mapcar #'show-cell r)
	     (terpri)))
    (mapcar #'show-row grid)))

(defun get-fixed-vals (cells)
  (iter
    (:for c :in cells)
    (if (eql (cell-ctype c) :fixed)
	(:collect (cell-cvalue c)))))

(defun prune-cell (c values)
  (case (cell-ctype c)
    (:fixed c)
    (:possible (let ((remaining (set-difference (cell-cvalue c) values)))
		 (case (length remaining)
		   (0 nil)
		   (1 (make-cell
		       :ctype :fixed
		       :cvalue (first remaining)))
		   (t (make-cell
		       :ctype :possible
		       :cvalue remaining)))))))

(defun prune-cells (cells)
  (let* ((fixeds (get-fixed-vals cells))
	 (new-values (iter
		       (for c in cells)
		       (let ((pruned (prune-cell c fixeds)))
			 (collect pruned)))))
    (when (not (member nil new-values))
      new-values)))

(defun transpose (grid)
  (apply #'mapcar #'list grid))



